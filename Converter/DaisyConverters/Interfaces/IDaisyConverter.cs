﻿using Newtonsoft.Json.Linq;
using System.IO;

namespace Converter.Converters
{
    public interface IDaisyConverter
    {
        Stream JsonToStream(JObject jObject);

        JObject StreamToJson(Stream stream);
    }
}
