using Converter.Converters;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using System;
using System.IO;
using System.Reflection;

namespace Converter
{
    public class Startup
    {
        private readonly string swaggerName = "FMIS Converter";
        private readonly string swaggerVersion = "v1";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped(typeof(IDaisyConverter), typeof(DaisyConverter));

            services.AddControllers().AddNewtonsoftJson(options => options.UseMemberCasing());

            services.AddControllers();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc(swaggerVersion, new OpenApiInfo
                {
                    Title = swaggerName,
                    Version = swaggerVersion,
                    Description = $"This '{swaggerName}' service was made by " +
                    "Niklaes Jacobsen (s160198) & " +
                    "Sebastian S�rensen (s170423). " +
                    "It was made for the project 'Farm Management Information System " +
                    "incorporating Nitrogen Sensor' at DTU. " +
                    "Our supervisor was Ian Bridgwood",
                });

                // Set the comments path for the Swagger JSON and UI.
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();

                app.UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint(
                        "/swagger/v1/swagger.json",
                        swaggerName + " " + swaggerVersion);
                    c.RoutePrefix = string.Empty;
                }
                );
            }

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
