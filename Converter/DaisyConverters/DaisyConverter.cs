﻿using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.IO;
using System.Text;
using Converter.Models;
using System.Collections.Generic;
using System;
using System.Linq;

namespace Converter.Converters
{
    public class DaisyConverter : IDaisyConverter
    {
        private string _templatePath;
        public DaisyConverter(string templatePath = "DaisyConverters/Templates/Zone_ref.dai")
        {
            this._templatePath = templatePath;
        }
        public JObject StreamToJson(Stream stream)
        {
            JObject jObject = new()
            {
                { "stream", stream.ToString() }
            };

            return jObject;
        }

        public Stream JsonToStream(JObject jObject)
        {
            List<Defaction> defactionList = JsonConvert.DeserializeObject<List<Defaction>>(jObject.GetValue("defactions").ToString());

            List<(string, string)> replacements = new();

            // Sort the zoneFiles
            foreach(var defaction in defactionList)
            {
                defaction.Events.Sort((x, y) => DateTime.Compare(x.Date, y.Date));
            }

            // Sort the defactions
            defactionList.Sort((x, y) => DateTime.Compare(x.Events.Last().Date, y.Events.Last().Date));
            
            DateTime startDate = new DateTime(defactionList.First().Events.First().Date.Year, 1, 1);
            DateTime endDate = defactionList.Last().Events.Last().Date;


            replacements.Add(("DC_FIELD_NAME", jObject["field_name"].ToString()));

            replacements.Add(("DC_ZONE_NUMBER", jObject.GetValue("name").ToString()));

            replacements.Add(("DC_START_DATE", $"{startDate.Year} {startDate.Month} {startDate.Day}"));

            replacements.Add(("DC_STOP_DATE", $"{endDate.Year} {endDate.Month} {endDate.Day}"));

            replacements.Add(("ALL_DEFACTION_NAMES", GetDefactionNames(defactionList)));

            replacements.Add(("DC_DEFACTION_AREA", GenerateDefactions(defactionList, startDate)));


            string fileString = File.ReadAllText(_templatePath);

            foreach (var replace in replacements)
            {
                fileString = fileString.Replace(replace.Item1, replace.Item2);
            }

            MemoryStream stringInMemoryStream = new(Encoding.UTF8.GetBytes(fileString));

            return stringInMemoryStream;
        }

        private static string GenerateDefactions(List<Defaction> defactions, DateTime startDate)
        {
            DateTime currentDate = startDate;
            StringBuilder stringBuilder = new();

            foreach (var defaction in defactions)
            {
                stringBuilder.AppendLine($"(defaction \"{defaction.Name}\" activity");

                foreach(var dEvent in defaction.Events) {
                    // if the currentdate is later than event date, add wait_mm_dd
                    if (dEvent.Date > currentDate)
                    {
                            if (dEvent.Type != "harvest")
                                stringBuilder.AppendLine($"(wait_mm_dd {dEvent.Date.Month} {dEvent.Date.Day})");
                            currentDate = dEvent.Date;
                    }

                    switch (dEvent.Type)
                    {
                        case "fertilize":
                            int nitrogenLevel = dEvent.NitrogenLevel < 1 ? 1: dEvent.NitrogenLevel; // Cannot be 0
                            string fertilize = $"(fertilize ({dEvent.FertilizationType ?? "NPK01"} (weight {nitrogenLevel} [kg N/ha])))";
                            stringBuilder.AppendLine(fertilize);
                            break;

                        case "sow":
                            string cropType = dEvent.CropType.Length == 0 ? dEvent.CropType : "SB_Normal";
                            string sow = $"(sow \"{cropType}\")";
                            stringBuilder.AppendLine(sow);
                            break;

                        case "harvest":
                            // TODO: stubStemAndLeaf & ripe is not standard, but will be here in this prototype
                            string stubStemAndLeaf = "(stub 10[cm])(stem 1.0[])(leaf 1.0 [])";

                            cropType = dEvent.CropType.Length == 0 ? dEvent.CropType : "SB_Normal";

                            // TODO: ripe is not standard, but will maybe needed in the future
                            // The ripe period is also just hardcoded here
                            string ripe1 = $"(wait ( or (crop_ds_after \"{cropType}\" 2.0) ; Ripe";
                            string ripe2 = $"(mm_dd {dEvent.Date.Month} {dEvent.Date.Day})))";
                            
                            string harvest = $"(harvest \"{cropType}\" {stubStemAndLeaf})";
                            stringBuilder.AppendLine(ripe1);
                            stringBuilder.AppendLine(ripe2);
                            stringBuilder.AppendLine(harvest);
                            break;

                        case "seed_bed_preparation":
                            stringBuilder.AppendLine("(seed_bed_preparation)");
                            break;

                        case "stubble_cultivation":
                            stringBuilder.AppendLine("(stubble_cultivation)");
                            break;

                        case "plowing":
                            stringBuilder.AppendLine("(plowing)");
                            break;

                        default:
                            stringBuilder.AppendLine($";; Event type {dEvent.Type} is not supported, therefore not added");
                            break;
                    }

                }

                stringBuilder.AppendLine($")");
            }
            return stringBuilder.ToString();
        }

        private static string GetDefactionNames(List<Defaction> defactions)
        {
            string defactionNames = "";

            foreach(var defaction in defactions)
            {
                var startSpace = defactionNames.Length == 0 ? string.Empty : " ";
                defactionNames += $"{startSpace}\"{defaction.Name}\"";
            }
            return defactionNames;
        }
    }
}
