﻿using Converter.Converters;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;

namespace Converter.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DaisyConverterController : ControllerBase
    {
        private readonly IDaisyConverter _converter;
        public DaisyConverterController(IDaisyConverter converter)
        {
            _converter = converter;
        }

        /// <summary>
        /// Convert Json -> Management file
        /// </summary>
        /// <param name="jObject"></param>
        /// <param name="fileName"></param>
        /// <remarks>
        /// Can only generate a management file from json
        /// Sample request:
        ///
        ///     POST /from-json/to-management-file
        ///     {
        ///         "field_name": "Taastrup mark",
        ///         "name": "53",
        ///         "defactions": [
        ///             {
        ///                 "id": "25610ff9-a1b7-46ab-8575-b1c344e25be6",
        ///                 "name": "SB_Normal 2019",
        ///                 "events": [
        ///                     {
        ///                         "id": "e706e4e9-8c24-4392-9ca4-207b5ab6d3e2",
        ///                         "type": "fertilize",
        ///                         "fertilization_type": "N25S",
        ///                         "crop_type": "",
        ///                         "nitrogen_level": 123,
        ///                         "date": "2019-06-18T00:00:00Z"
        ///                     }
        ///                 ]
        ///             },
        ///             {
        ///                 "id": "c2921861-4e03-467c-bf3c-aca5b48e4616",
        ///                 "name": "SB_Normal 2020",
        ///                 "events": [
        ///                     {
        ///                         "id": "4ecd5df4-1fbe-458e-8bdb-babc987c1fdc",
        ///                         "type": "fertilize",
        ///                         "fertilization_type": "N25S",
        ///                         "crop_type": "",
        ///                         "nitrogen_level": 123,
        ///                         "date": "2020-06-18T00:00:00Z"
        ///                     },
        ///                     {
        ///                         "id": "babc987c-458e-1fbe-9bab-4ecd5df4dfg4",
        ///                         "type": "fertilize",
        ///                         "fertilization_type": "N25S",
        ///                         "crop_type": "",
        ///                         "nitrogen_level": 123,
        ///                         "date": "2020-06-16T00:00:00Z"
        ///                     }
        ///                 ]
        ///             }
        ///         ]
        ///     }
        /// </remarks>
        /// <returns>A json representation of the managementFile</returns>
        /// <response code="200">Returns the newly created item</response>
        /// <response code="400">If the file uploaded is not a .dai file, or no file were uploaded</response>
        /// <response code="500">If any other error occured</response>
        [HttpPost]
        [Route("from-json/to-management-file")]
        public ActionResult PostConvertFromJson([FromBody] JObject jObject, [FromQuery] string fileName)
        {
            return File(_converter.JsonToStream(jObject), contentType: "application/octet-stream", fileDownloadName: fileName ?? "daisy.dai");
        }

        /// <summary>
        /// *[Not Implemented]* Convert Management file -> Json
        /// </summary>
        ///<param name="daisyFile"></param>
        /// <remarks>
        /// **BE AWARE THAT THIS SERVICE IS NOT IMPLEMENTED, AND THEREFORE ALWAYS RETURNS 501 NOT IMPLEMENTED**
        /// </remarks>
        /// <returns>A json representation of the managementFile</returns>
        /// <response code="200">Returns the newly created item</response>
        /// <response code="400">If the file uploaded is not a .dai file, or no file were uploaded</response>
        /// <response code="500">If any other error occured</response>
        [HttpPost]
        [Route("from-management-file/to-json")]
        public ActionResult PostConvertFromDaisy(IFormFile daisyFile)
        {
            // No file, throw error
            if (daisyFile == null)
                return BadRequest("The request did not contain a file");
            // Not a daisy file, throw error
            else if (!daisyFile.FileName.Contains(".dai"))
                return BadRequest("Not a .dai file");

            return StatusCode(501, "NOT IMPLEMENTED");

            // Not implemented
            //var JObject = _converter.StreamToJson(daisyFile.OpenReadStream());
            //return Ok(JObject);
        }
    }
}
