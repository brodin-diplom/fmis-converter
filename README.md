# FMIS Converter

This is a Converter Service.

## How to Windows / linux / mac:
- (sudo) docker build -t fmis-converter .
- (sudo) docker run --rm -it -p 80:80 -e ASPNETCORE_URLS="http://+:80" fmis-converter
 
## How to deploy on Google Cloud Run

### Cloud run init 
- Make sure you have Storage Object Admin & Stoage Object Creator roles in google cloud: https://console.cloud.google.com/iam-admin/iam?project=nitrogen-sensor-fmis
- Download glcoud: https://cloud.google.com/sdk/docs/install
- Run 'gcloud init', and follow steps
- Run 'gcloud auth configure-docker'

### After first run
- Create the docker image: 
	docker build -f Dockerfile -t gcr.io/nitrogen-sensor-fmis/fmis-converter .

- Push the docker image with the tag: 
	docker push gcr.io/nitrogen-sensor-fmis/fmis-converter

- Run gcloud deploy: 
	gcloud run deploy fmis-converter --project nitrogen-sensor-fmis --image gcr.io/nitrogen-sensor-fmis/fmis-converter --allow-unauthenticated --port 80 --cpu 1 --memory 1024Mi --concurrency 80 --timeout 300 --region europe-west6

- Now your instance is up and running
