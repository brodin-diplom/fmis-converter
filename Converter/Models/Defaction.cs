﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Converter.Models
{
    public class Defaction
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("events")]
        public List<Event> Events { get; set; }
    }
}
