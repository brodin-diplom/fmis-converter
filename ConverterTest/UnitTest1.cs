using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ConverterTest
{
    using Converter.Converters;
    using Newtonsoft.Json.Linq;
    using System.IO;

    namespace ConverterTest
    {
        [TestClass]
        public class Tests
        {
            private IDaisyConverter converter = new DaisyConverter(@"Templates\Zone_ref.dai");

            [TestMethod]
            public void ShouldConvert()
            {
                string testJson = "{ \"field_name\": \"Taastrup mark\", \"name\": \"53\", \"defactions\": [ { \"id\": \"25610ff9-a1b7-46ab-8575-b1c344e25be6\", \"name\": \"SB_Normal 2019\", \"events\": [ { \"id\": \"e706e4e9-8c24-4392-9ca4-207b5ab6d3e2\", \"type\": \"fertilize\", \"fertilization_type\": \"N25S\", \"crop_type\": \"\", \"nitrogen_level\": 123, \"date\": \"2019-06-18T00:00:00Z\" } ] }, { \"id\": \"c2921861-4e03-467c-bf3c-aca5b48e4616\", \"name\": \"SB_Normal 2020\", \"events\": [ { \"id\": \"4ecd5df4-1fbe-458e-8bdb-babc987c1fdc\", \"type\": \"fertilize\", \"fertilization_type\": \"N25S\", \"crop_type\": \"\", \"nitrogen_level\": 123, \"date\": \"2020-06-18T00:00:00Z\" }, { \"id\": \"babc987c-458e-1fbe-9bab-4ecd5df4dfg4\", \"type\": \"fertilize\", \"fertilization_type\": \"N25S\", \"crop_type\": \"\", \"nitrogen_level\": 123, \"date\": \"2020-06-16T00:00:00Z\" } ] } ] }";
                var testObject = JObject.Parse(testJson);
                var stream = converter.JsonToStream(testObject);

                string compareFile = File.ReadAllText(@"Results/test.dai");
                StreamReader streamReader = new(stream);
                string createdFile = streamReader.ReadToEnd();

                Assert.Fail();
                Assert.AreEqual(compareFile, createdFile);
            }
        }
    }




}