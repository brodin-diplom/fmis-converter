﻿using System;
using Newtonsoft.Json;

namespace Converter.Models
{
    public class Event
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("fertilization_type")]
        public string FertilizationType { get; set; }

        [JsonProperty("crop_type")]
        public string CropType { get; set; }

        [JsonProperty("nitrogen_level")]
        public int NitrogenLevel { get; set; }

        [JsonProperty("date")]
        public DateTime Date { get; set; }
    }
}